package com.example.mentalspace;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import java.util.List;

public class CustomAdapter extends BaseAdapter {
    private Context context;
    private List<Goal> itemList;
    private LayoutInflater inflater;

    public CustomAdapter(Context context, List<Goal> itemList) {
        this.context = context;
        this.itemList = itemList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_with_checkbox, parent, false);
        }

        TextView textView = convertView.findViewById(R.id.textViewItem);
        CheckBox checkBox = convertView.findViewById(R.id.checkBoxItem);

        Goal goal = itemList.get(position);
        textView.setText(goal.getTitle());
        checkBox.setChecked(goal.isCompleted());


        textView.setOnClickListener(v -> {
            Intent intent = new Intent(context, EditGoalsActivity.class);
            intent.putExtra("ID", goal.getId());
            context.startActivity(intent);
//            Toast.makeText(context, "Edit Goal", Toast.LENGTH_SHORT).show();

        });

        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            goal.setCompleted(isChecked);

            GoalDAO goalDAO = new GoalDAO(this.context  );
            int res = goalDAO.updateGoal(goal);


            Intent intent = new Intent(context, AllGoalsActivity.class);
            context.startActivity(intent);
        });

        return convertView;
    }
}