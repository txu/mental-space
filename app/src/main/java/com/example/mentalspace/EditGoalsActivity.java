package com.example.mentalspace;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import android.widget.DatePicker;

public class EditGoalsActivity extends AppCompatActivity {
    private EditText goalTitle;
    private EditText goalDesc;
    private DatePicker datePicker;

    private int editGoalId;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_editgoals);

        editGoalId = getIntent().getIntExtra(  "ID", 0);


        GoalDAO goalDAO = new GoalDAO(this);
        Goal goal = goalDAO.getGoal(editGoalId);

        goalTitle = findViewById(R.id.goaltitle);
        goalTitle.setText(goal.getTitle());

        goalDesc = findViewById(R.id.goal_desc);
        goalDesc.setText(goal.getDescription());

        datePicker = findViewById(R.id.date_picker);
        datePicker.updateDate(goal.getDate().getYear(), goal.getDate().getMonth(), goal.getDate().getDay());

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) ->
                {
                    Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
                    v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
                    return insets;
                }
        );
        ImageButton btnMenu = findViewById(R.id.editMenuBtn);
        btnMenu.setOnClickListener(v -> {
            Intent intent = new Intent(EditGoalsActivity.this, MenuActivity.class);
            startActivity(intent);
        });


        ImageButton backGoalsBtn = findViewById(R.id.back_to_goals);
        backGoalsBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                startActivity(new Intent(EditGoalsActivity.this, AllGoalsActivity.class));
            }
        });
    }

    public void onSaveClick(View view) {
        String goalTitleText = goalTitle.getText().toString();
        String goalDescText = goalDesc.getText().toString();
        Date d = new Date(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());


        Goal goal = new Goal(editGoalId, d, goalTitleText, goalDescText, false);
        GoalDAO goalDAO = new GoalDAO(this);
        int res = goalDAO.updateGoal(goal);

        startActivity(new Intent(EditGoalsActivity.this, AllGoalsActivity.class));

    }

    public void onDeleteClick(View view) {


        GoalDAO goalDAO = new GoalDAO(this);
        goalDAO.deleteGoal(editGoalId);
        startActivity(new Intent(EditGoalsActivity.this, AllGoalsActivity.class));

    }


}

