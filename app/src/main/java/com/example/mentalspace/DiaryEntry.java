package com.example.mentalspace;
import java.util.Date;
public class DiaryEntry {
    private int id;
    private int moodMoon;
    private Date date;
    private String entry;

    //constructor
    public DiaryEntry(int id, int moodMoon, Date date, String entry)
    {
        this.id = id;
        this.moodMoon = moodMoon;
        this.date = date;
        this.entry = entry;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public int getMoodMoon()
    {
        return moodMoon;
    }

    public void setMoodMoon(int moodMoon)
    {
        this.moodMoon = moodMoon;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public String getEntry()
    {
        return entry;
    }

    public void setEntry(String entry)
    {
        this.entry = entry;
    }
}
