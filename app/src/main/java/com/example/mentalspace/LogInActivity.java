package com.example.mentalspace;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class LogInActivity extends AppCompatActivity {
    EditText name, pwd;
    Button btnLogin, btnReg;
    Usersql usersql;
    SQLiteDatabase db;

    SharedPreferences sp1, sp2;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_log_in);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.Login), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        name = this.findViewById(R.id.name);
        pwd = this.findViewById(R.id.pwd);
        btnLogin = this.findViewById(R.id.login);
        btnReg = this.findViewById(R.id.reg);
        sp1 = this.getSharedPreferences("useinfo", this.MODE_PRIVATE);
        sp2 = this.getSharedPreferences("username", this.MODE_PRIVATE);

        name.setText(sp1.getString("usname", null));
        pwd.setText(sp1.getString("uspwd", null));
        usersql = new Usersql(this, "Userinfo", null, 1);      //construct db
        db = usersql.getReadableDatabase();
        btnLogin.setOnClickListener(v -> {
            String username = name.getText().toString();
            String password = pwd.getText().toString();
            Cursor cursor = db.query("logins", new String[]{"usname", "uspwd"}, " usname=? and uspwd=?", new String[]{username, password}, null, null, null);

            int flag = cursor.getCount();
            if (flag != 0) { //user existed
                Intent intent = new Intent();
                intent.setClass(LogInActivity.this, MainActivity.class); // jump to homepage
                SharedPreferences.Editor editor = sp2.edit();
                cursor.moveToFirst();
                String loginname = cursor.getString(0);
                editor.putString("Loginname", loginname);
                editor.apply();//save user name to SharedPreferences
                SharedPreferences.Editor editor2 = sp1.edit();
                editor2.putString("usname", loginname);
                editor2.putString("uspwd", password);
                editor2.apply();
                startActivity(intent);
            } else {
                Toast.makeText(LogInActivity.this, "Wrong user name or password！", Toast.LENGTH_LONG).show();
            }
        });

        btnReg.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.setClass(LogInActivity.this, RegisterActivity.class);
            startActivity(intent);
            Toast.makeText(LogInActivity.this, "Go to the Register Page", Toast.LENGTH_SHORT).show();
        });


    }
}