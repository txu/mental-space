package com.example.mentalspace;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import java.util.Date;
import android.widget.DatePicker;

public class GoalsActivity extends AppCompatActivity {
    private EditText goalTitle;
    private EditText goalDesc;
    private DatePicker datePicker;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_goals);
        goalTitle = findViewById(R.id.goaltitle);
        goalDesc = findViewById(R.id.goal_desc);
        datePicker = findViewById(R.id.date_picker);

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.maingoals), (v, insets) ->
                {
                    Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
                    v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
                    return insets;
                }
        );

        ImageButton btnMenu = findViewById(R.id.goalsMenuBtn);
        btnMenu.setOnClickListener(v -> {
            Intent intent = new Intent(GoalsActivity.this, MenuActivity.class);
            startActivity(intent);
        });

        ImageButton backGoalsBtn = findViewById(R.id.back_to_goals);
        backGoalsBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                startActivity(new Intent(GoalsActivity.this, AllGoalsActivity.class));
            }
        });
    }

    public void onSaveClick(View view) {
        String goalTitleText = goalTitle.getText().toString();
        String goalDescText = goalDesc.getText().toString();
        Date d = new Date(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());

        Goal goal = new Goal(d, goalTitleText, goalDescText, false);
        GoalDAO goalDAO = new GoalDAO(this);
        goalDAO.addGoal(goal);

        startActivity(new Intent(GoalsActivity.this, AllGoalsActivity.class));
    }




}
