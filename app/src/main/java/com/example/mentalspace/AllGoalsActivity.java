package com.example.mentalspace;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.util.ArrayList;

public class AllGoalsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_allgoals);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        ImageButton btnMenu = findViewById(R.id.allgoalsMenuBtn);
        btnMenu.setOnClickListener(v -> {
            Intent intent = new Intent(AllGoalsActivity.this, MenuActivity.class);
            startActivity(intent);
        });

        ListView listView = findViewById(R.id.unchecked_goals);
        ArrayList<GoalListViewItem> items = new ArrayList<>();

        GoalDAO goalDAO = new GoalDAO(this);
        ArrayList<Goal> allGoals = (ArrayList<Goal>) goalDAO.getCheckedGoals(false);


        for (Goal singleGoal : allGoals) {
            GoalListViewItem item = new GoalListViewItem(singleGoal.getTitle(), singleGoal.getId());
            items.add(item);

        }

        CustomAdapter unCheckedAdapter = new CustomAdapter(this, allGoals);
        listView.setAdapter(unCheckedAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GoalListViewItem clickedItem = (GoalListViewItem) parent.getItemAtPosition(position);
                Intent intent = new Intent(AllGoalsActivity.this, EditGoalsActivity.class);
                intent.putExtra("ID", clickedItem.getNumber());
                startActivity(intent);
            }

        });
        ImageButton backGoalsBtn = findViewById(R.id.back_to_goals);
        backGoalsBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                startActivity(new Intent(AllGoalsActivity.this, MainActivity.class));
            }
        });

        ListView checkedListView = findViewById(R.id.checked_goals);
        ArrayList<Goal> allCheckedGoals = (ArrayList<Goal>) goalDAO.getCheckedGoals(true);;
        CustomAdapter checkedAdapter = new CustomAdapter(this, allCheckedGoals);
        checkedListView.setAdapter(checkedAdapter);


        Button goalBtn = findViewById(R.id.addgoalbutton);
        goalBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(new Intent(AllGoalsActivity.this, GoalsActivity.class));
            }
        });

        ImageView btnHome = findViewById(R.id.diaryhomebutton);
        btnHome.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                startActivity(new Intent(AllGoalsActivity.this, MainActivity.class));
            }


        });
    }
}


