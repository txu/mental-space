package com.example.mentalspace;


public class Article {
    private String title;
    private String authors;
    private String text;

    // Constructor
    public Article(String title, String authors, String text) {
        this.title = title;
        this.authors = authors;
        this.text = text;
    }

    // Getters
    public String getTitle() {
        return title;
    }

    public String getAuthors() {
        return authors;
    }

    public String getText() {
        return text;
    }
}
