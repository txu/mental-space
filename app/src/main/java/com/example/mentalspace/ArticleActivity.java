package com.example.mentalspace;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import java.util.List;
import android.widget.SearchView;






public class ArticleActivity extends AppCompatActivity {

    private ListView listView;
    private ArticleAdapter articleAdapter;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_article);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.InfoMain), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        listView = findViewById(R.id.articles_search);
        searchView = findViewById(R.id.search_view);

        List<Article> articles = ArticleLoader.loadArticles(this);
        articleAdapter = new ArticleAdapter(this, articles);
        listView.setAdapter(articleAdapter);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                articleAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                articleAdapter.getFilter().filter(newText);
                return false;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Article article = (Article) articleAdapter.getItem(position);
                Intent intent = new Intent(ArticleActivity.this, ArticleDetailActivity.class);
                intent.putExtra("title", article.getTitle());
                intent.putExtra("authors", article.getAuthors());
                intent.putExtra("text", article.getText());
                startActivity(intent);
            }
        });


        ImageView btnArticleHome = findViewById(R.id.homeButton);
        btnArticleHome.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view) {

                startActivity(new Intent(ArticleActivity.this, MainActivity.class));
            }
        });

        ImageButton btnMenu = findViewById(R.id.articlesMenuBtn);
        btnMenu.setOnClickListener(v -> {
            Intent intent = new Intent(ArticleActivity.this, MenuActivity.class);
            startActivity(intent);
        });

        ImageView btnArticleBack = findViewById(R.id.articleBackButton);
        btnArticleBack.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view) {

                startActivity(new Intent(ArticleActivity.this, InfoActivity.class));
            }
        });

    }


}