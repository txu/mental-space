package com.example.mentalspace;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;


public class ArticleDetailActivity extends AppCompatActivity {

    private TextView titleTextView;
    private TextView authorsTextView;
    private TextView articleTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_article_detail);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        titleTextView = findViewById(R.id.titleTextView);
        authorsTextView = findViewById(R.id.authorsTextView);
        articleTextView = findViewById(R.id.articleTextView);

        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        String authors = intent.getStringExtra("authors");
        String textFileName = intent.getStringExtra("text");

        titleTextView.setText(title);
        authorsTextView.setText(authors);
        articleTextView.setText(loadTextFromAsset(textFileName));

        ImageView btnInfoHome = findViewById(R.id.homeButton);
        btnInfoHome.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view) {

                startActivity(new Intent(ArticleDetailActivity.this, MainActivity.class));
            }
        });

        ImageButton btnMenu = findViewById(R.id.articleDetailBtn);
        btnMenu.setOnClickListener(v -> {
            Intent intent2 = new Intent(ArticleDetailActivity.this, MenuActivity.class);
            startActivity(intent2);
        });

        ImageView btnInfoBack = findViewById(R.id.detailBackButton);
        btnInfoBack.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view) {

                startActivity(new Intent(ArticleDetailActivity.this, ArticleActivity.class));
            }
        });
    }

    private String loadTextFromAsset(String fileName) {
        String text = "";
        try {
            InputStream is = getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            text = new String(buffer, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return text;
    }


}