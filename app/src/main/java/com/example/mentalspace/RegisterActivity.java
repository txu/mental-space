package com.example.mentalspace;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends AppCompatActivity {
    EditText username, pwd, pwd2;
    Button submit, toLog;
    Usersql usersql;
    SQLiteDatabase db;
    SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_register);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        // set Mental Space bold
        TextView textViewWelcome = findViewById(R.id.textView_welcome);
        String fullText = "Welcome to Mental Space!";
        SpannableString spannableString = new SpannableString(fullText);

        int start = fullText.indexOf("Mental Space");
        int end = start + "Mental Space".length();
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        textViewWelcome.setText(spannableString);

        username = this.findViewById(R.id.username);
        pwd = this.findViewById(R.id.userpwd);
        pwd2 = this.findViewById(R.id.userpwd2);
        submit = this.findViewById(R.id.submit);
        toLog = this.findViewById(R.id.to_login);
        usersql = new Usersql(this, "Userinfo", null, 1);
        db = usersql.getReadableDatabase();
        sp = this.getSharedPreferences("useinfo", this.MODE_PRIVATE);

        toLog.setOnClickListener(v -> {

            Intent intent = new Intent();
            intent.setClass(RegisterActivity.this, LogInActivity.class);
            startActivity(intent);
        });

        submit.setOnClickListener(new View.OnClickListener() {
            boolean flag = true;  //user existed

            @Override
            public void onClick(View v) {
                String name = username.getText().toString();
                String pwd01 = pwd.getText().toString();
                String pwd02 = pwd2.getText().toString();
                if (name.equals("") || pwd01.equals("") || pwd02.equals("")) {
                    Toast.makeText(RegisterActivity.this, "User name or password cannot be empty!", Toast.LENGTH_LONG).show();
                } else {
                    //user already existed
                    Cursor cursor = db.query("logins", new String[]{"usname"}, null, null, null, null, null);

                    while (cursor.moveToNext()) {
                        if (cursor.getString(0).equals(name)) {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        if (pwd01.equals(pwd02)) {
                            ContentValues cv = new ContentValues();
                            cv.put("usname", name);
                            cv.put("uspwd", pwd01);
                            db.insert("logins", null, cv);
                            SharedPreferences.Editor editor = sp.edit();
                            editor.putString("usname", name);
                            editor.putString("uspwd", pwd01);
                            editor.apply();
                            Intent intent = new Intent();
                            intent.setClass(RegisterActivity.this, MainActivity.class);
                            startActivity(intent);
                            Toast.makeText(RegisterActivity.this, "Register accomplished！", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(RegisterActivity.this, "Password inconsistent！", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(RegisterActivity.this, "User existed！", Toast.LENGTH_LONG).show();
                    }

                }
            }


        });
    }
}