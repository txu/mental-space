package com.example.mentalspace;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class DiaryActivity extends AppCompatActivity {
    private DiaryEntryDAO diaryEntryDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_diary);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.Diary), (v, insets) ->
        {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);

            diaryEntryDAO = new DiaryEntryDAO(this);

            //example usage
            //.. ..

            return insets;
        });

        ImageView btnHome = findViewById(R.id.diaryhomebutton);
        btnHome.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view) {

                startActivity(new Intent(DiaryActivity.this, MainActivity.class));
            }
        });

        ImageButton btnMenu = findViewById(R.id.diaryMenuBtn);
        btnMenu.setOnClickListener(v -> {
            Intent intent = new Intent(DiaryActivity.this, MenuActivity.class);
            startActivity(intent);
        });

        ImageButton backGoalsBtn = findViewById(R.id.back_to_main);
        backGoalsBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                startActivity(new Intent(DiaryActivity.this, MainActivity.class));
            }
        });

        Button btnRef = findViewById(R.id.ref_button);
        btnRef.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                startActivity(new Intent(DiaryActivity.this, ReflectionActivity.class));
            }
        });
    }
}
