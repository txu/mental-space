package com.example.mentalspace;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DiaryEntryDAO {
    private DatabaseHelper dbHelper;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    public DiaryEntryDAO(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public long addDiaryEntry(DiaryEntry diaryEntry) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_MOODMOON, diaryEntry.getMoodMoon());
        values.put(DatabaseHelper.COLUMN_DATE, dateFormat.format(diaryEntry.getDate()));
        values.put(DatabaseHelper.COLUMN_ENTRY, diaryEntry.getEntry());

        long id = db.insert(DatabaseHelper.TABLE_DIARYENTRY, null, values);
        db.close();
        return id;
    }

    public DiaryEntry getDiaryEntry(int id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(DatabaseHelper.TABLE_DIARYENTRY, new String[]{
                        DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_MOODMOON, DatabaseHelper.COLUMN_DATE, DatabaseHelper.COLUMN_ENTRY},
                DatabaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(id)}, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
            try {
                DiaryEntry diaryEntry = new DiaryEntry(
                        cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ID)),
                        cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_MOODMOON)),
                        dateFormat.parse(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_DATE))),
                        cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ENTRY))
                );
                cursor.close();
                db.close();
                return diaryEntry;
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        db.close();
        return null;
    }

    public List<DiaryEntry> getAllDiaryEntries() {
        List<DiaryEntry> diaryEntries = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(DatabaseHelper.TABLE_DIARYENTRY, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                try {
                    DiaryEntry diaryEntry = new DiaryEntry(
                            cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ID)),
                            cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_MOODMOON)),
                            dateFormat.parse(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_DATE))),
                            cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ENTRY))
                    );
                    diaryEntries.add(diaryEntry);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return diaryEntries;
    }

    public int updateDiaryEntry(DiaryEntry diaryEntry) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_TITLE, diaryEntry.getMoodMoon());
        values.put(DatabaseHelper.COLUMN_DATE, dateFormat.format(diaryEntry.getDate()));
        values.put(DatabaseHelper.COLUMN_DESCRIPTION, diaryEntry.getEntry());

        int rowsAffected = db.update(DatabaseHelper.TABLE_DIARYENTRY, values, DatabaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(diaryEntry.getId())});
        db.close();
        return rowsAffected;
    }

    public void deleteDiaryEntry(int id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(DatabaseHelper.TABLE_DIARYENTRY, DatabaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(id)});
        db.close();
    }
}
