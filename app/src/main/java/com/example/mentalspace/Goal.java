package com.example.mentalspace;
import java.util.Date;

public class Goal {
    private int id;
    private Date date;
    private String title;
    private String description;
    private boolean completed;

    //Constructor
    public Goal(int id, Date date, String title, String description, boolean completed)
    {
        this.id = id;
        this.date = date;
        this.title = title;
        this.description = description;
        this.completed = completed;
    }

    public Goal(Date date, String title, String description, boolean completed)
    {
        this.date = date;
        this.title = title;
        this.description = description;
        this.completed = completed;
    }

    //Getters and Setters
    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        this.date = date;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public boolean isCompleted()
    {
        return completed;
    }

    public void setCompleted(boolean completed)
    {
        this.completed = completed;
    }
}
