package com.example.mentalspace;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MenuActivity extends AppCompatActivity {

    private Usersql usersql;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_menu);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.menu_layout), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        this.usersql = new Usersql(this, "Userinfo", null, 1);
        this.db = usersql.getWritableDatabase();

        Button resetBtn = findViewById(R.id.bt_reset_pswd);
        resetBtn.setOnClickListener(v -> showResetPasswordDialog());
        ImageButton btnRt = findViewById(R.id.bt_menu_back);
        Button btnAboutApp = findViewById(R.id.bt_aboutapp);
        Button btnContact = findViewById(R.id.bt_contact);
        Button btnLogout=findViewById(R.id.bt_logout);
        Button btnReport = findViewById(R.id.bt_report);

        btnRt.setOnClickListener(v -> finish());
        btnAboutApp.setOnClickListener(v->showAboutDialog());
        btnContact.setOnClickListener(v->showCallDialog());
        btnReport.setOnClickListener(v->showEmailDialog());
        btnLogout.setOnClickListener(v -> startActivity(new Intent(MenuActivity.this, LogInActivity.class)));
    }
    private String loadAssetTextAsString(String fileName) {
        BufferedReader in = null;
        try {
            StringBuilder buf = new StringBuilder();
            InputStream is = getAssets().open(fileName);
            in = new BufferedReader(new InputStreamReader(is));

            String str;
            boolean isFirst = true;
            while ((str = in.readLine()) != null) {
                if (isFirst)
                    isFirst = false;
                else
                    buf.append('\n');
                buf.append(str);
            }
            return buf.toString();
        } catch (IOException e) {
            Log.e("MainActivity", "Error opening asset " + fileName);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e("MainActivity", "Error closing asset " + fileName);
                }
            }
        }
        return null;
    }

    private void showAboutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AlertDialogCustom);
        builder.setTitle("About the App");
        builder.setMessage(loadAssetTextAsString("about_app.txt"));
        builder.setPositiveButton("OK", (dialog, which) -> dialog.dismiss());
        AlertDialog dialog = builder.create();
        dialog.show();
        TextView messageView = dialog.findViewById(android.R.id.message);
        if (messageView != null) {
            messageView.setTextColor(getResources().getColor(R.color.textfaded));
        }
    }
    private void showCallDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AlertDialogCustom2);
        builder.setTitle("Contact Helpdesk");
        builder.setMessage("Do you want to call 1234 5678?");

        builder.setPositiveButton("Call", (dialog, which) -> dialPhoneNumber("12345678"));

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void showEmailDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogCustom2);
        builder.setTitle("Report Issues");
        builder.setMessage("Do you want to send an email to issue-reports@mental-space.com?");

        builder.setPositiveButton("Send Email", (dialog, which) -> sendEmail("issues-report@mental-space.com"));

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void sendEmail(String emailAddress) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailAddress});

        try {
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("QueryPermissionsNeeded")
    private void dialPhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
        else {
            Log.e("DialPhoneNumber", "No activity found to handle dial intent.");
            Toast.makeText(this, "No app found to handle dial intent.", Toast.LENGTH_LONG).show();
        }
    }

    public void showResetPasswordDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AlertDialogCustom2);
        View view = getLayoutInflater().inflate(R.layout.reset_pwd_dialog, null);
        EditText newPassword = view.findViewById(R.id.edit_new_password);
        EditText confirmPassword = view.findViewById(R.id.edit_confirm_password);
        TextView passwordError = view.findViewById(R.id.text_password_error);

        builder.setView(view);
        builder.setTitle("Reset Password");
        builder.setPositiveButton("Reset", null);
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.dismiss());

        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(dialogInterface -> {
            Button button = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            button.setOnClickListener(view1 -> {
                String newPass = newPassword.getText().toString();
                String confirmPass = confirmPassword.getText().toString();
                if (!newPass.equals(confirmPass)) {
                    passwordError.setVisibility(View.VISIBLE);
                } else {
                    passwordError.setVisibility(View.GONE);
                    updatePasswordInDatabase(newPass);
                    dialog.dismiss();
                }
            });
        });
        dialog.show();
    }

    private void updatePasswordInDatabase(String newPassword) {
        SQLiteDatabase db = usersql.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("uspwd", newPassword);

        SharedPreferences sp = getSharedPreferences("useinfo", MODE_PRIVATE);
        String username = sp.getString("usname", null);
        db.update("logins", values, "usname=?", new String[]{username});
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("uspwd",newPassword);
        editor.apply();
        Toast.makeText(this, "Password successfully updated.", Toast.LENGTH_SHORT).show();
    }

}