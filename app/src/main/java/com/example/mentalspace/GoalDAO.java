package com.example.mentalspace;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class GoalDAO {
    private DatabaseHelper dbHelper;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    public GoalDAO(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public long addGoal(Goal goal) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_DATE, dateFormat.format(goal.getDate()));
        values.put(DatabaseHelper.COLUMN_TITLE, goal.getTitle());
        values.put(DatabaseHelper.COLUMN_DESCRIPTION, goal.getDescription());
        values.put(DatabaseHelper.COLUMN_COMPLETED, goal.isCompleted() ? 1 : 0);

        long id = db.insert(DatabaseHelper.TABLE_GOAL, null, values);
        db.close();
        return id;
    }

    public Goal getGoal(int id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(DatabaseHelper.TABLE_GOAL, new String[]{
                        DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_DATE, DatabaseHelper.COLUMN_TITLE, DatabaseHelper.COLUMN_DESCRIPTION, DatabaseHelper.COLUMN_COMPLETED},
                DatabaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(id)}, null, null, null);

        if (cursor != null) {
            cursor.moveToFirst();
            try {
                Goal goal = new Goal(
                        cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ID)),
                        dateFormat.parse(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_DATE))),
                        cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_TITLE)),
                        cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_DESCRIPTION)),
                        cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_COMPLETED)) > 0
                );
                cursor.close();
                db.close();
                return goal;
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        db.close();
        return null;
    }

    public List<Goal> getAllGoals() {
        List<Goal> goals = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(DatabaseHelper.TABLE_GOAL, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                try {
                    Goal goal = new Goal(
                            cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ID)),
                            dateFormat.parse(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_DATE))),
                            cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_TITLE)),
                            cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_DESCRIPTION)),
                            cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_COMPLETED)) > 0
                    );
                    goals.add(goal);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return goals;
    }

    public int updateGoal(Goal goal) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.COLUMN_DATE, dateFormat.format(goal.getDate()));
        values.put(DatabaseHelper.COLUMN_TITLE, goal.getTitle());
        values.put(DatabaseHelper.COLUMN_DESCRIPTION, goal.getDescription());
        values.put(DatabaseHelper.COLUMN_COMPLETED, goal.isCompleted() ? 1 : 0);

        int rowsAffected = db.update(DatabaseHelper.TABLE_GOAL, values, DatabaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(goal.getId())});
        db.close();
        return rowsAffected;
    }

    public void deleteGoal(int id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(DatabaseHelper.TABLE_GOAL, DatabaseHelper.COLUMN_ID + "=?", new String[]{String.valueOf(id)});
        db.close();
    }


    public List<Goal> getCheckedGoals(boolean isChecked) {
        List<Goal> goals = new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String selection = DatabaseHelper.COLUMN_COMPLETED + " = ?";
        String[] selectionArgs = {isChecked ? "1" : "0"}; // SQLite does not have a separate boolean type. It uses 0 for false and 1 for true.

        Cursor cursor = db.query(
                DatabaseHelper.TABLE_GOAL,
                null,
                selection,
                selectionArgs,
                null,
                null,
                null);

        if (cursor.moveToFirst()) {
            do {
                try {
                    Goal goal = new Goal(
                            cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_ID)),
                            dateFormat.parse(cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_DATE))),
                            cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_TITLE)),
                            cursor.getString(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_DESCRIPTION)),
                            cursor.getInt(cursor.getColumnIndexOrThrow(DatabaseHelper.COLUMN_COMPLETED)) > 0
                    );
                    goals.add(goal);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return goals;
    }
}
