
package com.example.mentalspace;

public class GoalListViewItem {
    private String text;
    private int number;

    public GoalListViewItem(String text, int number) {
        this.text = text;
        this.number = number;
    }

    public String getText() {
        return text;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return text;
    }
}