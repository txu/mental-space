package com.example.mentalspace;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_info);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.InfoMain), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        ImageButton btnMenu = findViewById(R.id.infoMenuBtn);
        btnMenu.setOnClickListener(v -> {
            Intent intent = new Intent(InfoActivity.this, MenuActivity.class);
            startActivity(intent);
        });

        Button btnArt = findViewById(R.id.bt_articles);
        btnArt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create an Intent to start the SecondActivity

                Intent intent = new Intent(InfoActivity.this, ArticleActivity.class);
                startActivity(intent);
            }
        });

        ImageView btnInfoHome = findViewById(R.id.homeButton);
        btnInfoHome.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view) {

                startActivity(new Intent(InfoActivity.this, MainActivity.class));
            }
        });

        ImageView btnInfoBack = findViewById(R.id.infoBackButton);
        btnInfoBack.setOnClickListener(new View.OnClickListener(){

            public void onClick(View view) {

                startActivity(new Intent(InfoActivity.this, MainActivity.class));
            }
        });


    }
}