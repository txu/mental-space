package com.example.mentalspace;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import android.widget.TextView;
import android.widget.Toast;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class ReflectionActivity extends AppCompatActivity {


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_reflectionactivity);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.Ref), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        ImageButton moon1 = findViewById(R.id.imageButton1);
        ImageButton moon2 = findViewById(R.id.imageButton2);
        ImageButton moon3 = findViewById(R.id.imageButton3);
        ImageButton moon4 = findViewById(R.id.imageButton4);
        ImageButton moon5 = findViewById(R.id.imageButton5);

        TextView date = findViewById(R.id.reflectiondate);

        LocalDateTime currentDateTime = LocalDateTime.now( );
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMMM, yyyy");
        String formattedDateTime = currentDateTime.format(formatter);
        date.setText(formattedDateTime);

        Button saveRef = findViewById(R.id.saveref);
        ImageButton backRef = findViewById(R.id.backref);

        ImageButton btnMenu = findViewById(R.id.refMenuBtn);
        btnMenu.setOnClickListener(v -> {
            Intent intent = new Intent(ReflectionActivity.this, MenuActivity.class);
            startActivity(intent);
        });

        ImageButton btnHome = findViewById(R.id.refhomebutton);
        btnHome.setOnClickListener(v -> {
            Intent intent = new Intent(ReflectionActivity.this, MainActivity.class);
            startActivity(intent);
        });

        saveRef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ReflectionActivity.this, DiaryActivity.class));
            }
        });

        backRef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ReflectionActivity.this, DiaryActivity.class));
            }
        });


        moon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moon1.setBackgroundResource(R.drawable.moon1sel);
                moon2.setBackgroundResource(R.drawable.moon2unsel);
                moon3.setBackgroundResource(R.drawable.moon3unsel);
                moon4.setBackgroundResource(R.drawable.moon4unsel);
                moon5.setBackgroundResource(R.drawable.moon5unsel);
            }
        });
//
//
//
        moon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moon1.setBackgroundResource(R.drawable.moon1unsel);
                moon2.setBackgroundResource(R.drawable.moon2sel);
                moon3.setBackgroundResource(R.drawable.moon3unsel);
                moon4.setBackgroundResource(R.drawable.moon4unsel);
                moon5.setBackgroundResource(R.drawable.moon5unsel);
            }
        });
//
        moon3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moon1.setBackgroundResource(R.drawable.moon1unsel);
                moon2.setBackgroundResource(R.drawable.moon2unsel);
                moon3.setBackgroundResource(R.drawable.moon3sel);
                moon4.setBackgroundResource(R.drawable.moon4unsel);
                moon5.setBackgroundResource(R.drawable.moon5unsel);
            }
        });


        moon4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moon1.setBackgroundResource(R.drawable.moon1unsel);
                moon2.setBackgroundResource(R.drawable.moon2unsel);
                moon3.setBackgroundResource(R.drawable.moon3unsel);
                moon4.setBackgroundResource(R.drawable.moon4sel);
                moon5.setBackgroundResource(R.drawable.moon5unsel);
            }
        });

        moon5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moon1.setBackgroundResource(R.drawable.moon1unsel);
                moon2.setBackgroundResource(R.drawable.moon2unsel);
                moon3.setBackgroundResource(R.drawable.moon3unsel);
                moon4.setBackgroundResource(R.drawable.moon4unsel);
                moon5.setBackgroundResource(R.drawable.moon5sel);
            }
        });



    }
}
