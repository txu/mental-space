package com.example.mentalspace;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private GoalDAO goalDAO;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);

            goalDAO = new GoalDAO(this);

            // Example usage
            Goal newGoal = new Goal(0, new Date(), "Complete project", "Finish the Android project", false);
            long goalId = goalDAO.addGoal(newGoal);

            Goal goal = goalDAO.getGoal((int) goalId);
            if (goal != null) {
                Log.d("MainActivity", "Goal: " + goal.getTitle() + ", Description: " + goal.getDescription());
            }

            List<Goal> allGoals = goalDAO.getAllGoals();
            for (Goal g : allGoals) {
                Log.d("MainActivity", "Goal: " + g.getTitle() + ", Description: " + g.getDescription());
            }

            goal.setCompleted(true);
            goalDAO.updateGoal(goal);

            goalDAO.deleteGoal(goal.getId());

            return insets;
        });


        Button btnIn = findViewById(R.id.bt_info);
        Button btnDi = findViewById(R.id.bt_diary);
        Button btnGo = findViewById(R.id.bt_goals);
        ImageButton btnMenu = findViewById(R.id.menu_button);
        btnMenu.setOnClickListener(v -> {
            Intent intent = new Intent(MainActivity.this, MenuActivity.class);
            startActivity(intent);
        });
        btnIn.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, InfoActivity.class)));

        btnDi.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, DiaryActivity.class)));


        btnGo.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, AllGoalsActivity.class)));


    }
}