package com.example.mentalspace;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper
{
    private static final String DATABASE_NAME = "MentalSpaceDatabase";
    private static final int DATABASE_VERSION = 1;

    // Goal table
    public static final String TABLE_GOAL = "goals";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_COMPLETED = "completed";

    // SQL query to create the goal table
    private static final String TABLE_CREATE_GOAL =
            "CREATE TABLE " + TABLE_GOAL + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_DATE + " TEXT, " +
                    COLUMN_TITLE + " TEXT, " +
                    COLUMN_DESCRIPTION + " TEXT, " +
                    COLUMN_COMPLETED + " INTEGER);";

    // Diary table
    public static final String TABLE_DIARYENTRY = "diaryEntries";
    public static final String COLUMN_MOODMOON = "moodMoon";
    public static final String COLUMN_ENTRY = "entry";

    private static final String TABLE_CREATE_DIARYENTRY =
            "CREATE TABLE " + TABLE_DIARYENTRY + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COLUMN_MOODMOON + " INTEGER, " +
                    COLUMN_DATE + " TEXT, " +
                    COLUMN_ENTRY + " TEXT);";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE_GOAL);
        db.execSQL(TABLE_CREATE_DIARYENTRY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GOAL);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DIARYENTRY);
        onCreate(db);
    }
}
