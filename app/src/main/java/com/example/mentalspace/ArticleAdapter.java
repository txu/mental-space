package com.example.mentalspace;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.List;
import android.widget.Filter;
import android.widget.Filterable;
import java.util.ArrayList;


public class ArticleAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private List<Article> articles;
    private List<Article> filteredArticles;
    private LayoutInflater inflater;

    public ArticleAdapter(Context context, List<Article> articles) {
        this.context = context;
        this.articles = articles;
        this.filteredArticles = new ArrayList<>(articles);
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return filteredArticles.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredArticles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.article_list, parent, false);
            holder = new ViewHolder();
            holder.titleTextView = convertView.findViewById(R.id.text_view_title);
            holder.authorsTextView = convertView.findViewById(R.id.text_view_authors);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Article article = filteredArticles.get(position);
        holder.titleTextView.setText(article.getTitle());
        holder.authorsTextView.setText(article.getAuthors());

        return convertView;
    }

    private static class ViewHolder {
        TextView titleTextView;
        TextView authorsTextView;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<Article> filteredList = new ArrayList<>();

                if (constraint == null || constraint.length() == 0) {
                    filteredList.addAll(articles);
                } else {
                    String filterPattern = constraint.toString().toLowerCase().trim();

                    for (Article article : articles) {
                        if (article.getTitle().toLowerCase().contains(filterPattern) ||
                                article.getAuthors().toLowerCase().contains(filterPattern)) {
                            filteredList.add(article);
                        }
                    }
                }

                results.values = filteredList;
                results.count = filteredList.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                filteredArticles.clear();
                filteredArticles.addAll((List<Article>) results.values);
                notifyDataSetChanged();
            }
        };
    }
}
