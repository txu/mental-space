package com.example.mentalspace;

import android.content.Context;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ArticleLoader {

    public static List<Article> loadArticles(Context context) {
        List<Article> articles = new ArrayList<>();
        try {
            InputStream is = context.getAssets().open("articles.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            String json = new String(buffer, StandardCharsets.UTF_8);
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = jsonObject.getJSONArray("Articles");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject articleObject = jsonArray.getJSONObject(i);
                String title = articleObject.getString("Title");
                String authors = articleObject.getString("Authors");
                String text = articleObject.getString("Text");

                articles.add(new Article(title, authors, text));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return articles;
    }
}
