Introduction

Anxiety disorders are the most prevalent psychiatric disorders and are associated with a high burden of illness. With a 12-month prevalence of 10.3%, specific (isolated) phobias are the most common anxiety disorders, although persons suffering from isolated phobias rarely seek treatment. Panic disorder with or without agoraphobia (PDA) is the next most common type with a prevalence of 6.0%, followed by social anxiety disorder (SAD, also called social phobia; 2.7%) and generalized anxiety disorder (GAD; 2.2%). Evidence is lacking on whether these disorders have become more frequent in recent decades. Women are 1.5 to two times more likely than men to receive a diagnosis of anxiety disorder.

The age of onset for anxiety disorders differs among the disorders. Separation anxiety disorder and specific phobia start during childhood, with a median age of onset of 7 years, followed by SAD (13 years), agoraphobia without panic attacks (20 years), and panic disorder (24 years). GAD may start even later in life. Anxiety disorders tend to run a chronic course, with symptoms fluctuating in severity between periods of relapse and remission in GAD and PDA and a more chronic course in SAD. After the age of 50, a marked decrease in the prevalence of anxiety disorders has been observed in epidemiological studies. GAD is the only anxiety disorder that is still common in people aged 50 years or more.

The current conceptualization of the etiology of anxiety disorders includes an interaction of psychosocial factors, eg, childhood adversity, stress, or trauma, and a genetic vulnerability, which manifests in neurobiological and neuropsychological dysfunctions. The evidence for potential biomarkers for anxiety disorders in the fields of neuroimaging, genetics, neurochemistry, neurophysiology, and neurocognition has been summarized in two recent consensus papers. Despite comprehensive, high-quality neurobiological research in the field of anxiety disorders, these reviews indicate that specific biomarkers for anxiety disorders have yet to be identified. Thus, it is difficult to give recommendations for specific biomarkers (eg, genetic polymorphisms) that could help identify persons at risk for an anxiety disorder.

Obsessive-compulsive disorder (OCD) and post-traumatic stress disorder (PTSD) were formerly included in the anxiety disorders, but have now been placed in other chapters in the fifth edition of the Diagnostic and Statistical Manual of Mental Disorders (DSM-5). Therefore, OCD and PTSD are not discussed in this review.


Diagnosis

Anxiety disorders are often underdiagnosed in primary care. In DSM-5, the group of anxiety disorders has been expanded to include separation anxiety disorder, a diagnosis the previous DSM version reserved for children only. The change was based on the findings of epidemiological studies that revealed the unexpectedly high prevalence of the condition in adults. DSM-5 also introduces selective mutism—the failure of children to speak in special social situations—and a new term called illness anxiety disorder, defined by excessive preoccupation and fear of having a serious medical illness. Illness anxiety disorder was formerly called hypochondriasis in DSM-IV and Tenth Revision of the International Statistical Classification of Diseases and Related Health Problems (ICD-10); in DSM-5, it is not classified under anxiety disorders but belongs to the Somatic Symptom and Related Disorders category. In the current ICD-11 Beta Draft, hypochondriasis is placed in the group Obsessive-Compulsive or Related Disorders. It is characterized by catastrophic misinterpretation of bodily symptoms and is manifest as obsessive and excessive health-related behaviors. The fear of having a serious medical condition persists despite thorough medical evaluation and repeated reassurance that the patient does not suffer from the feared illness.

Mixed anxiety and depression is a category listed only in ICD-10 and not in DSM-5. It is often diagnosed in primary care. Research on the treatment of this disorder is limited. Adjustment disorder with mixed anxiety and depressed mood (F43.22) is a condition with similar symptomatology. It occurs as a reaction to stressful life events.

The differential diagnosis of anxiety disorders includes common mental disorders, such as other anxiety disorders, major depression, and somatic symptom disorders, as well as physical illnesses such as coronary heart or lung diseases, hyperthyroidism, and others.

Anxiety disorders often co-occur with other anxiety disorders, major depression, somatic symptom disorders, personality disorders, and substance abuse disorders. For example, major depression was found to be highly correlated with all anxiety disorders in a large European survey (eg, with GAD, the odds ratio was 33.7; with panic disorder, it was 29.4). Anxiety disorders were also strongly interrelated: GAD was highly associated with agoraphobia (25.7), panic disorder (20.3), and SAD (13.5).

To determine the severity of anxiety disorders and to monitor treatment progress, commonly used rating scales can be used, including the Hamilton Anxiety Scale (HAM-A) for GAD, the Panic and Agoraphobia Scale (PAS) for panic disorder/agoraphobia, and the Liebowitz Social Anxiety Scale (LSAS) for SAD.


Treatment

Alice is a case vignette of the treatment of a patient with GAD.

Alice, a 48-year-old female dentist, presented to a psychiatrist with a 7-month history of anxiety symptoms, which included persistent feelings of restlessness, irritability, difficulty concentrating, sleep disturbance, fatigue, nausea, diarrhea, muscle cramps, and the sensation of having a lump in her throat. She was suffering from constant worry that her husband could become ill or might have an accident while driving to work. Her symptoms resulted in frequent absenteeism, which caused significant problems at work. Her medical history was unremarkable. The psychiatrist diagnosed her with generalized anxiety disorder, DSM-5 F41.1.

Four weeks previously, Alice had been prescribed the benzodiazepine diazepam by her general practitioner, and initially took it as prescribed. Although it helped with her anxiety, she felt that it made her feel dull and worried that it would interfere with her work as a dentist. She kept thinking that she would become addicted to the drug and stopped the intake.

The psychiatrist started treatment with the serotonin-norepinephrine reuptake inhibitor venlafaxine. Because the patient was sensitive to side effects, the drug was started with 37.5 mg/d for 3 days. Then, the dose was increased to 75 mg/d. She reported mild nausea and fatigue; however, it was not clear whether this was due to the medication or to the illness.

After another 2 weeks, these adverse effects resolved, and the dose was increased to 225 mg/d. The patient also received weekly sessions of cognitive behavioral therapy. Symptoms of GAD were resolved almost completely after 7 weeks. The psychiatrist advised Alice to continue on venlafaxine for at least 6 months. Then, the drug was slowly tapered, by reducing the dose to 150 mg/d for 1 month, then to 75 mg/d for another month. Then, after 2 weeks on 37.5 mg/d, the medication was stopped. The patient did not report relevant withdrawal symptoms and did no show reoccurrence of significant anxiety symptoms during a follow-up observation period of almost 1 year.

In clinical settings, most patients seeking help suffer from GAD, PDA, and SAD. Not all anxiety disorders have to be treated when symptoms are mild, transient, and without associated impairment in social and occupational function. However, treatment is indicated when a patient shows marked distress or suffers from complications resulting from the disorder (eg, secondary depression, suicidal ideation, or alcohol abuse).

Anxiety disorders can be treated mostly on an outpatient basis. Indications for hospitalization include suicidality, unresponsiveness to standard treatments, or relevant comorbidity, eg, with major depression, personality disorders, or substance abuse.

The treatment recommendations in this article are based on guidelines for anxiety disorders. For such guidelines, a systematic literature search for randomized clinical trials was performed. Studies were analyzed by using internationally acknowledged quality assessment tools, and the recommendations were reviewed by expert panels.

Patients with different anxiety disorders show different degrees of health care utilization. For example, in the United States, 54.4% of patients with PDA, but only 27.3% of patients with specific phobias, contacted health care services in 1 year. Whereas patients with PDA often fear that they suffer from a severe somatic disorder, such as a myocardial infarction, and that they need immediate medical help, people with simple phobias often have the feeling that they can cope with the disorder or even think it is “normal” to have a fear of spiders or dogs.

There is evidence for substantial undertreatment of anxiety disorders. In a large European study, only 20.6% of participants with an anxiety disorder sought professional help. Of those participants who contacted health care services, 23.2% received no treatment at all, 19.6% received only psychological treatment, 30.8% received only drug treatment, and 26.5% were treated both with drugs and psychotherapy. Likewise, a Dutch study in primary care found that only 27% of patients with anxiety disorders received guideline-orientated care.

Patients should receive “psychoeducation” about their diagnosis, the possible etiology, and the mechanisms of action of the available treatment approaches. The treatment plan should include psychotherapy, pharmacotherapy, and other interventions, which should be chosen after careful consideration of individual factors, eg, the patient's preference, the patient's history with previous treatment attempts, illness severity, comorbidities such as personality disorders, suicidally, local availability of treatment methods, wait time for psychotherapy appointments, costs, and other factors.


Pharmacotherapy

Whereas many studies have shown the efficacy of medications for GAD, PDA, and SAD, there are very few studies on drug treatment for specific phobias, for example, there is a small study suggesting the efficacy of paroxetine.
Selective serotonin reuptake inhibitors and selective serotonin norepinephrine reuptake inhibitors
Due to their positive benefit/risk balance, selective serotonin reuptake inhibitors (SSRIs) and selective serotonin norepinephrine reuptake inhibitors (SNRIs are recommended as first-line drugs. Patients should be informed that the onset of the anxiolytic effect of these antidepressants has a latency of 2 to 4 weeks (in some cases up to 6 weeks). During the first 2 weeks, adverse effects may be stronger. Initial jitteriness or an increase in anxiety symptoms may occur, which may reduce the patients' treatment compliance. Lowering the starting dose of the antidepressants may reduce these adverse effects. A review of studies in depressed patients suggested that SNRIs may be less well tolerated than the SSRIs. However, according to clinical experience, tolerability may differ among patients, and it is also possible that an individual patient may experience less adverse effects when switched from an SSRI to an SNRI.

Some SSRIs and SNRIs are inhibitors of cytochrome P450 enzymes and hence may interact with other psychopharmacological drugs and medications for medical illnesses. After stopping treatment with an SSRI, withdrawal reactions may occur. However, these are much less frequent and severe than the withdrawal reactions observed after terminating benzodiazepine treatment. These adverse reactions may be more frequent with paroxetine than with sertraline or fluoxetine.


Pregabalin

Pregabalin is a calcium modulator, acting at the α2δ subunit of voltage-gated calcium channels. The drug has sedating properties. Sleep disorders, which are common in patients with anxiety disorders, may improve earlier with pregabalin than with the SSRIs or SNRIs. Onset of efficacy is earlier with pregabalin than with antidepressants. Pregabalin is not subject to hepatic metabolism and hence does not interact with inhibitors or inducers of cytochrome P450 enzymes. However, there have been concerns about the abuse of pregabalin in individuals suffering from substance abuse and also withdrawal syndromes after abrupt discontinuation.


Tricyclic antidepressants

The traditional tricyclic antidepressants (TCAs) imipramine and clomipramine are as effective as second-generation antidepressants in the treatment of anxiety disorders. In general, the frequency of adverse events is higher for TCAs than for SSRIs or SNRIs. Thus, these drugs should be tried first before TCAs are used. The dosage should be uptitrated slowly until dosage levels reach those used in the treatment of depression. TCAs should be used with caution in patients considered to be at risk of suicide, due to their potential fatal toxicity after overdose.


Buspirone

Buspirone, a 5-hydroxytryptamine receptor 1A (5HT1A) agonist, has been shown in some controlled studies to be effective in the treatment of GAD. However, not all studies have shown superiority to placebo and/or equivalence to standard drugs.


Benzodiazepines

The anxiolytic effects of benzodiazepines begin soon after oral or parenteral application. In contrast to antidepressants, benzodiazepines do not lead to initially increased jitteriness and insomnia. In the United States, 55% to 94% of patients with anxiety disorders are treated with benzodiazepines. Likewise, European studies have shown a high rate of long-term benzodiazepine use. However, benzodiazepine treatment may be associated with central nervous system (CNS) depression, resulting in fatigue, dizziness, increased reaction time, impaired driving skills, and other adverse effects. Cognitive functions may be impaired, mainly in elderly patients. After longterm treatment with benzodiazepines (eg, over 4 to 8 months), dependency may occur in some patients, especially in patients predisposed for substance abuse. Tolerance (resulting in a patient's constant desire to increase the dose) seems to be rare. Thus, the risks and benefits should be carefully considered before treatment with benzodiazepine. Current guidelines do not recommend benzodiazepines as first-line treatments. The recommendations to give preference to newer antidepressants are not based on direct comparison studies but rather on the known risks of benzodiazepines.

In exceptional cases (eg, severe cardiac disease, contraindications for the standard drugs, suicidality, and other conditions), benzodiazepines can be used for a limited time period. However, patients with a history of benzodiazepine or other substance abuse should be excluded from treatment. Benzodiazepines may also be used in combination with SSRIs/ SNRIs during the first weeks before the onset of efficacy of the antidepressants. Cognitive behavioral therapy (CBT) may facilitate benzodiazepine withdrawal.

In singular cases, acute panic attacks may require immediate drug treatment. In that case, lorazepam melting tablets at a dose of 1.0 to 2.5 mg may be given as needed (up to a dose no higher than 7.5 mg/d). It is usually sufficient to talk calmly with the patient and explain that the attack is not due to a life-threatening medical condition.

In contrast to SSRIs and SNRIs, benzodiazepines do not treat depression, which is a common comorbid condition in anxiety disorders.


Moclobemide

Moclobemide is a selective and reversible inhibitor of monoamine oxidase A. It is used in the treatment of SAD. Because not all studies have shown evidence for superiority to placebo, the drug is not recommended as first-line treatment.


Other drugs

Some other drugs have shown efficacy in anxiety disorders in randomized controlled studies but are not licensed for the treatment of these disorders in most countries. Medicolegal issues have to be considered whenever drugs that have not been approved for anxiety indications are prescribed off label.


Agomelatine

The antidepressant agomelatine—which acts as an agonist for melatonin MT1 and MT2 receptors and as an antagonist for serotonin 5-HT2C receptors—was shown to be effective in four studies in GAD. However, the drug is only licensed for the treatment of major depression, not for GAD. With agomelatine, discontinuation symptoms and sexual dysfunction are less likely than with SSRI or SNRI antidepressants. Elevations of hepatic enzymes occur in around 1 % of treated patients.


Quetiapine

The atypical antipsychotic quetiapine was shown to be effective in a number of studies in GAD. It is usually prescribed in the treatment of schizophrenic psychoses in dosages between 150 and 800 mg/d. For the treatment of anxiety, lower doses (50 to 300 mg/day) are required. However, probably due to adverse effects such as the metabolic syndrome, the drug was not licensed for anxiety disorders in most countries. In general, typical adverse events, such as sedation or weight gain, were less frequent in patients receiving lower doses. Quetiapine can only be used off-label in treatment-refractory patients. The onset of efficacy is earlier than with antidepressants.


Vortioxetine

A new antidepressant, vortioxetine, was investigated in several controlled studies in GAD. However, according to a meta-analysis, significant improvement for vortioxetine could not be demonstrated compared with placebo.


Phytotherapy

Some controlled studies have shown the efficacy of an orally taken lavender oil preparation in GAD and mixed anxiety/depression. It has yet to be established whether lavender oil is as effective as standard treatments. The comparison studies only used low doses of the comparators, eg, 20 mg paroxetine per day or one tablet of lorazepam 0.5 mg per day, which may have led to insufficient efficacy of the comparison drugs.

Studies with Kava-kava (Piper methysticum) showed inconsistent results, and the extract was been withdrawn from the market in some countries due to hepatotoxicity in some preparations. Valerian extract was not effective in placebo-controlled studies in anxious patients. Also, St John's wort was not effective in SAD.

Some other phytotherapeutics have been investigated in individuals with anxiety conditions. Due to the low quality of these studies, the evidence for the investigated products is not sufficient. Standardization may be an issue in herbal preparations. For example, it was shown that different preparations of St. John's wort exhibited large differences in the content of the putatively effective ingredients.


Relative efficacy of drugs

In a meta-analysis of all available drug studies in anxiety disorders, the pre -post effect sizes of the different drugs were determined. We simply looked at the absolute difference in anxiety scale scores before and after treatment, without regard to the relative efficacy compared with placebo. This approach makes it possible to include hundreds of studies in comparisons of differential efficacy of all available drugs and not only the few direct head-to-head comparisons. From the patients' point of view, the improvement in anxiety symptoms as measured by the change from baseline to end point is more relevant than the difference from a control group.

The available medications for anxiety disorders showed considerably large differences in pre-post effect sizes. For example, the improvement achieved with the most efficacious drug (quetiapine; Cohen's d=339) was almost three times higher than what was accomplished with the drug with the weakest efficacy (buspirone; d=135). Quetiapine, however, is not licensed for the treatment of any anxiety disorder in most countries. Among the drugs showing high effect sizes and that are licensed for anxiety disorders and recommended by guidelines were the SSRIs escitalopram (d=2.75) and paroxetine (d=2.42), and the SNRIs venlafaxine (d=2.32) and pregabalin (2.30). Also, some benzodiazepines, eg, diazepam (d=2.46) and lorazepam (2.44), showed high effect sizes. However, these drugs are not recommended for routine treatment.


General treatment principles

Patients must be informed about possible adverse effects, interactions, safety warnings, and contraindications, as indicated in the current summary of product characteristics. If patients are educated about the possibility that some early side effects might later decrease in intensity, compliance may improve. Patients with anxiety disorders are often hesitant to take psychotropic drugs because they are afraid of adverse effects. In particular, patients with PDA may easily discontinue antidepressants because of initial jitteriness and nervousness.

In around 75% of the cases of doses for drug treatments, doses in the lower part of the therapeutic range are sufficient. In patients with severe hepatic impairment, a dosage adjustment or use of medications that are cleared primarily by the kidney (eg, pregabalin) may be required.

For all drugs recommended in this article, relapse prevention studies in at least one anxiety disorder have been conducted in patients who have responded to previous open treatment with a certain drug and were then randomized to placebo or ongoing blind treatment with the same drug for periods of between 6 and 18 months. All of these studies showed a significant advantage for staying on active medication when compared with switching to placebo. Based on the findings from these relapse prevention studies and clinical experiences, drug treatment should be continued for 12 months or more after remission has occurred. Given the chronic course of anxiety disorders, it is regrettable that there are almost no controlled studies that investigate treatment periods over 12 months. To avoid withdrawal syndromes, the dose should be slowly tapered off over a period of 2 weeks at treatment termination.

It is a common opinion that patients treated with drugs show immediate relapse after stopping medication, whereas gains of psychological therapies are maintained for months or years after treatment termination. This would offer psychological therapies considerable advantage over drug treatment. However, in naturalistic studies following up anxiety patients, substantial relapse rates were also found years after CBT treatment. For example, in an analysis of eight randomized controlled trials on CBT for anxiety disorders, 48% of patients were still symptomatic after 2 to 14 years of follow-up. On the other hand, in relapse prevention studies in which treatment responders to open drug treatment for 8 to 12 weeks were re-randomized to long-term treatment (24 to 52 months) with the same drug or to placebo, only around 40% of patients randomized to placebo relapsed.


Drug-drug interactions

When treating anxiety disorders with medications, drug interactions have to be monitored. SSRIs, such as fluoxetine, fluvoxamine, and paroxetine, are particularly liable to be involved in pharmacokinetic interactions, such as enzyme inhibition in the cytochrome P450 system. Additive CNS depression may occur when drugs with sedating properties are combined, eg, TCAs, benzodiazepines, or pregabalin, resulting in unwanted sedation, drowsiness, or increased reaction time. Additive effects at the neurotransmitter level can occur when medications are combined that have antagonistic effects on the same receptors, eg, two drugs with anticholinergic effects.


Unresponsiveness to standard treatments

Before considering a patient to be treatment unresponsive, it should be ascertained that the diagnosis was correct, adherence to the treatment plan was sufficient, the dose prescribed had covered the full range, and there had been a trial period of adequate duration. When patients report previous treatment failures, it often turns out that a drug was only prescribed in the lowest dose or was stopped within the first 2 weeks due to side effects that occurred in the initial phase before the patient could experience improvement.

Concurrent drugs may interfere with efficacy, eg, metabolic inhibitors or enhancers. Psychosocial factors may affect response, and comorbid personality or substance abuse disorders are especially likely to complicate anxiety disorders. When initial treatment fails, the physician has to decide when to change the treatment plan. There have been few systematic trials of treatment-refractory patients with anxiety disorders. If after treatment at what is considered an adequate dose for 4 to 6 weeks a patient shows no response, the medication should be changed. One analysis showed that the chance of responding beyond the fourth week was 20% or less if no effect had occurred by the second week of treatment, suggesting an even earlier switching of drugs. Although “switching studies” are lacking, many treatment-refractory patients are reported to respond when a different class of antidepressant is tried (eg, change from one SSRI to another SSRI, or to an SNRI, or vice versa). If partial response is seen after this period, there is still a chance that the patient will respond after another 4 to 6 weeks of therapy with increased dosages. For some antidepressants, the studies on a potential dose-response relationship are inconclusive, perhaps due to the lack of statistical power for showing a difference between lower and higher doses. According to clinical experience, however, a trial with a higher dose in patients with insufficient response is warranted.

Elderly patients may take longer to show a response. In patients who are unresponsive to psychotropic drugs, the addition of CBT is generally recommended.

A combination of antidepressants and benzodiazepines is sometimes used in treatment-refractory cases.

When all standard treatments have failed, the off-label use of drugs may be considered, for example, drugs licensed for another anxiety disorder or that are not licensed but have shown efficacy in clinical studies. Such drugs include quetiapine and agomelatine.


Treatment of GAD in older patients

With the exception of GAD, anxiety disorders are less common in patients over 65 years of age. Therefore, only a few studies for the treatment of GAD have been performed with older patients. Controlled studies have shown the efficacy of duloxetine, venlafaxine, pregabalin, and quetiapine in patients over 65 years old. In the elderly, an increased sensitivity to drug side effects and interactions must be considered, including anticholinergic effects, risk of orthostatic hypotension and cardiovascular events, risk of falling, and paradoxical reactions to benzodiazepines.

In the elderly, effect sizes for CBT tend to be somewhat smaller than those found in mixed-age populations. A meta-analysis of studies with older adults with GAD showed superiority of CBT to waitlist or “treatment-as-usual” conditions but not to active controls (eg, psychological or pill placebos).


Treating children and adolescents

Whereas specific phobias, SAD, and separation anxiety disorder are common in younger people, PDA and GAD are relatively rare. There are some randomized, placebo-controlled studies of pharmacotherapy for anxiety disorders in children and adolescents showing efficacy of sertraline, fluoxetine, and duloxetine in young patients with GAD, of venlafaxine and paroxetine in SAD, and of sertraline, fluvoxamine, and fluoxetine in mixed samples, including patients with separation anxiety disorder, GAD, and SAD. However, little is known about the value of long-term treatment. The combination of CBT and sertraline was found to be more effective than both treatment modalities alone.

There had been concerns about increased risk for suicidal ideation (not suicides) in children and adolescents treated for major depression with SSRIs (escitalopram, citalopram, paroxetine, and sertraline), mirtazapine, and venlafaxine. According to a meta-analysis, the risk:benefit ratio in the treatment of depressed children and adolescents seemed to be most favorable with fluoxetine. Although suicidal ideation is less common in anxiety disorders than in major depression, the risks of pharmacological treatment have to be weighed carefully against the risks of nontreatment. It was reported that antidepressant prescriptions for children and adolescents decreased substantially after European and US regulatory agencies issued warnings about a possible suicide risk with antidepressant use in pediatric patients in 2003/2004 and that these decreases were associated with increases in suicide rates in children and adolescents (although a causal relationship is not proven).

For children and youths with separation anxiety disorder, several treatment studies exist. However, no controlled studies on the treatment of adults with this disorder could be traced.

There is also a paucity of treatment studies for children with selective mutism. Small studies have shown that psychotherapeutic approaches were at least better than waitlist controls. One review indicated that only two very small placebo-controlled drug studies showed efficacy of the SSRIs fluoxetine and sertraline.


Pregnancy and breastfeeding

For pregnant women, the risk of an untreated anxiety disorder must be weighed against the risk of damage to the unborn child as a result of treatment. A large study suggested no substantial increase in the risk of cardiac malformations attributable to antidepressant use during the first trimester. However, antidepressants have been associated with increased risk of spontaneous abortions, stillbirths, early deliveries, respiratorydistress, and endocrine and metabolic dysfunctions. Nevertheless, the current evidence suggests that the use of many antidepressants, especially the SSRIs, is favorable compared with exposing the mother to the risks of untreated depression or anxiety disorders.

Likewise, a careful assessment of the risk/benefit balance has to be done when a mother is breastfeeding. In such cases, CBT should be considered as an alternative to medication treatment.


Psychotherapy

All patients with anxiety disorders require supportive talks and attention to the emotional problems that are associated with the anxiety disorder. Psychoeducation includes information about the physiology of the bodily symptoms of anxiety reactions and the rationale of available treatment possibilities. Many patients may require formal psychological treatment interventions, which are mostly done on an outpatient basis.

The treatment of anxiety disorders by CBT is described in more detail in the article by Borza in this issue of Dialogues in Clinical Neuroscience (p 203). The efficacy of CBT for all anxiety disorders has been shown in a large number of controlled studies. If avoidance of feared situations is a relevant factor in phobic disorders, exposure techniques should be included in the treatment schedule, in which patients are confronted with their feared situations.

In comparison with CBT the evidence for psychodynamic therapy is weaker. Controlled studies with psychodynamic therapy were markedly fewer in number, and of lower quality, than those with CBT, and some comparison studies have shown superiority of CBT.

For specific phobias, there are only studies with behavioral therapy, which should be performed as exposure treatment. In the available treatment studies, it was shown that only a few sessions (eg, one to five) were necessary for effective treatment of specific phobias.

In recent years, many studies have investigated psychological therapies that are performed via the Internet, usually involving minimal or no contact with a therapist. However, at present, evidence is lacking that these treatments are as effective as individual CBT with face-to-face contact. Internet therapies may be an option for regions in which psychotherapy is not widely available or to bridge the waiting period before a “real” therapy is scheduled to begin. They are also less expensive than face-to-face psychotherapies. However, important issues have to be solved, including reimbursement by health insurance systems, data protection, the problem of “remote diagnosis” without direct contact, assessment of suicidality, and medicolegal aspects. Treatments with a “virtual reality” setting may be a promising new approach for specific phobias.


Combining psychotherapy and medication

Both psychotherapy and pharmacotherapy have been shown to be more effective than control groups. However, whereas drugs are mostly compared with placebo controls, the evidence for psychotherapy in anxiety disorders is mainly based on comparisons with a “wait-list,“ a control method that was used in 70% to 75% of the studies in adults and children. Because pill placebos have higher effect sizes than waitlist controls, the effect size differences between active and control conditions cannot be compared between psychotherapy and medication studies. Therefore, our research group conducted a large meta-analysis of all available controlled short-term studies for anxiety disorders and compared the pre-post effect size differences (before and after treatment) between medications and psychotherapies. In this meta-analysis, which was based on studies with around 35 000 patients, medications were associated with a significantly higher average pre-post effect size (Cohen's d=2.02) than psychotherapies (d=1.22; P<0.0001). It was also found that patients included in psychotherapy studies were less severely ill than those recruited for medication trials.

Moreover, it was shown that the average pre-post effect sizes for pill placebos were of similar strength to the gains achieved with psychotherapies. This surprising finding cannot be explained by heterogeneity, publication bias, or by allegiance effects. However, this does not mean that psychotherapy is not helpful, as the average effect size obtained with psychotherapies is still strong—it only means that a placebo pill is a very powerful treatment, at least for the first weeks or months of treatment. Nevertheless, patients should be informed about the relative efficacy of the treatment options they are offered.

The meta-analysis also showed that combinations of psychotherapy and pharmacotherapy had a relatively high effect size (d=2.12). However, only a few combination studies were available for this comparison, and some of these have not been conducted with the most powerful drugs.


Other treatment options

Exercise (eg, aerobic training, such as jogging 5 km three times a week) has been studied in PDA. However, it was found that exercise was less effective than clomipramine and no more effective than a control condition, relaxation. Thus, exercise can only be recommended as adjunctive treatment to standard treatments.

Hypnosis, autogenic training, and biofeedback or complementary medicine methods such as acupuncture, osteopathy, or homeopathy are often recommended for the treatment of clinical anxiety. However, controlled studies fulfilling at least basic methodological standards are lacking.

Although controlled studies on the usefulness of self-help groups are lacking, patients should be encouraged to participate if appropriate.


Conclusions

GAD and other anxiety disorders are the most prevalent mental disorders. A large amount of data available from randomized controlled trials permits the formulation of robust evidence-based recommendations for the treatment of GAD, PDA, and SAD. In most cases, drug treatment and CBT may substantially improve quality of life in GAD patients.

In the past 3 years, B. Bandelow has served as a paid consultant to Lundbeck, Mundipharma, and Pfizer. He has received honoraria for lectures at scientific meetings and continuing medical education events from Lundbeck, Pfizer, and Servier and honoraria for scientific articles from Servier.